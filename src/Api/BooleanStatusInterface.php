<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\BooleanStatus\Api;

/**
 * Interface BooleanStatusInterface.
 *
 * Read / put / compare current boolean status.
 *
 * This is a convenience interface intended for implementors that provide read / put / compare status capabilities.
 *
 * @see BooleanStatusReadInterface
 * @see BooleanStatusCompareInterface
 * @see BooleanStatusPutInterface
 * @see BooleanStatusReadCompareTrait to help yourself to implement part of the functionality.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\BooleanStatus
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface BooleanStatusInterface extends
    BooleanStatusReadInterface,
    BooleanStatusCompareInterface,
    BooleanStatusPutInterface
{
}
