<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\BooleanStatus\Api;

use Interactiv4\Contracts\SPL\Exception\Wrapper\Api\ExceptionWrapperTrait;
use Interactiv4\Contracts\SPL\Status\Api\Exception\CouldNotCompareStatusException;
use Interactiv4\Contracts\SPL\Status\Api\Exception\CouldNotReadStatusException;

/**
 * Trait BooleanStatusReadCompareTrait.
 *
 * For classes implementing both BooleanStatusReadInterface and BooleanStatusCompareInterface:
 * - Directly
 * - By using BooleanStatusReadCompareInterface
 * - By using BooleanStatusInterface
 *
 * isStatus() method can be implemented in a generic way. Php does not allow generic method bodies in interfaces yet,
 * so you can help yourself by using this trait to implement functionality in previously described cases.
 *
 * @see BooleanStatusReadInterface
 * @see BooleanStatusCompareInterface
 * @see BooleanStatusReadCompareInterface
 * @see BooleanStatusInterface
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\BooleanStatus
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
trait BooleanStatusReadCompareTrait
{
    use ExceptionWrapperTrait;

    /**
     * {@inheritdoc}
     */
    public function isBooleanStatus(
        bool $status,
        array $context = []
    ): bool {
        /** @var BooleanStatusReadInterface|BooleanStatusReadCompareInterface|BooleanStatusInterface $this */
        try {
            $currentStatus = $this->isEnabled($context);
        } catch (CouldNotReadStatusException $e) {
            /** @noinspection PhpUnhandledExceptionInspection */
            $this->wrapAndThrowException(CouldNotCompareStatusException::class, $e);
        }

        return $status === $currentStatus;
    }
}
