<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\BooleanStatus\Api;

/**
 * Interface BooleanStatusReadCompareInterface.
 *
 * Read / compare current boolean status.
 * Most of times, an implementation providing BooleanStatusReadInterface capabilities,
 * is also able to perform the adequate comparison and provide BooleanStatusCompareInterface capabilities
 * by using the retrieved value.
 *
 * This is a convenience interface to avoid implementors having to implement separately both interfaces:
 *
 * @see BooleanStatusReadInterface
 * @see BooleanStatusCompareInterface
 * @see BooleanStatusReadCompareTrait to help yourself to implement part of the functionality.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\BooleanStatus
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface BooleanStatusReadCompareInterface extends
    BooleanStatusReadInterface,
    BooleanStatusCompareInterface
{
}
