<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\BooleanStatus\Api;

use Interactiv4\Contracts\SPL\Status\Api\Exception\CouldNotCompareStatusException;
use LogicException;
use RuntimeException;

/**
 * Interface BooleanStatusCompareInterface.
 *
 * Compare expected boolean status against current one.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\BooleanStatus
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface BooleanStatusCompareInterface
{
    /**
     * Compare expected boolean status against current one.
     * It MAY use optionally supplied context to determine how / where status should be retrieved / compared.
     * The context array can contain arbitrary data. There are not any assumptions that can be made by implementors.
     *
     * @param bool  $status  Mandatory.
     * @param array $context Optional, additional data to determine how / where status should be retrieved / compared.
     *
     * @return bool Returning a boolean without raising an exception is the way to communicate everything is ok.
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Db table lock when persisting status.
     *
     * @throws CouldNotCompareStatusException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    public function isBooleanStatus(
        bool $status,
        array $context = []
    ): bool;
}
