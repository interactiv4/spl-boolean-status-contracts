<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\BooleanStatus\Api;

use Interactiv4\Contracts\SPL\Status\Api\Exception\CouldNotPutStatusException;
use LogicException;
use RuntimeException;

/**
 * Interface BooleanStatusPutInterface.
 *
 * Store internally / persist boolean status.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\BooleanStatus
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface BooleanStatusPutInterface
{
    /**
     * Store internally / persist enabled status (true).
     * It MAY use optionally supplied context to determine how / where status should be stored / persisted.
     * The context array can contain arbitrary data. There are not any assumptions that can be made by implementors.
     *
     * @param array $context Optional, additional data to determine how / where status should be stored / persisted.
     *
     * @return void Returning without raising an exception is the way to communicate everything is ok.
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Db table lock when persisting status.
     *
     * @throws CouldNotPutStatusException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    public function enable(array $context = []): void;

    /**
     * Store internally / persist disabled status (false).
     * It MAY use optionally supplied context to determine how / where status should be stored / persisted.
     * The context array can contain arbitrary data. There are not any assumptions that can be made by implementors.
     *
     * @param array $context Optional, additional data to determine how / where status should be stored / persisted.
     *
     * @return void Returning without raising an exception is the way to communicate everything is ok.
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Db table lock when persisting status.
     *
     * @throws CouldNotPutStatusException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    public function disable(array $context = []): void;
}
