<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\SPL\BooleanStatus\Api;

use Interactiv4\Contracts\SPL\Status\Api\Exception\CouldNotReadStatusException;
use LogicException;
use RuntimeException;

/**
 * Interface BooleanStatusReadInterface.
 *
 * Read current boolean status.
 *
 * @api
 *
 * @package Interactiv4\Contracts\SPL\BooleanStatus
 *
 * @SuppressWarnings(PHPMD.NumberOfChildren)
 */
interface BooleanStatusReadInterface
{
    /**
     * Read current boolean status.
     * It MAY use optionally supplied context to determine how status should be retrieved.
     * The context array can contain arbitrary data. There are not any assumptions that can be made by implementors.
     *
     * @param $context Optional, additional data to determine how status should be retrieved.
     *
     * @return bool Current status.
     *
     * @throws RuntimeException
     * - When an error which can only happen at runtime occurs, e.g.: Db table lock when reading status.
     *
     * @throws CouldNotReadStatusException
     * - When an error that does not fit in previous exceptions occurs. It should lead to a code fix.
     * - @see LogicException and generic exceptions MUST be wrapped into this exception type.
     */
    public function isEnabled($context = []): bool;
}
